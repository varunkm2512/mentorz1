export class UserRegister {
    password?: string;
    phone_number?: PhoneNumber;
    device_info?: DeviceInfo;
    user_profile?: UserProfile;
}

export class PhoneNumber{
    cc?: number;
    iso_alpha_2_cc?: string;
    number?: string;
}

export class DeviceInfo{
    device_token?: string;
    device_type?: string;
}

export class UserProfile{
    birth_date?: string;
    name?: string;
    basic_info?: string;
    video_bio_hres?: string;
}