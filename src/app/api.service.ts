import { Injectable } from '@angular/core';
import { Http } from '@angular/http';


@Injectable()
export class ApiService {

  constructor(private http: Http) {}
   signUpDetails(values:any){
  //return this.http.post('https://mentorz-http.firebaseio.com/data.json',values);
    return this.http.put('http://stgapp.mentorz.com:8080/mentorz/api/v3/user',values);
    }

    loginDetails(values:any){
      return this.http.post("http://stgapp.mentorz.com:8080/mentorz/api/v2/user/login",values);
    }

    getDetails(){
    }

    //return this.http.get('http://stgapp.mentorz.com:8080/v1/user/signed/geturl/object/'); 
  
}
