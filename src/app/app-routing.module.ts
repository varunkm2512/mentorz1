import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './component/login/login.component';
import { HomeComponent} from './component/home/home.component';
import { StoriesComponent } from './component/stories/stories.component';
import { SigninComponent } from './component/signin/signin.component';
import { SetpasswordComponent } from './component/setpassword/setpassword.component';
const routes: Routes = [
{path:'', redirectTo: 'home', pathMatch:'full'},
{path:'home', component: HomeComponent },
{path:'login',component: LoginComponent },
{path:'stories', component: StoriesComponent},
{path:'signin', component:SigninComponent },
{path:'setpassword', component: SetpasswordComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
