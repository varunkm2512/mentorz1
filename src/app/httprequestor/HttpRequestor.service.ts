// Imports
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

// Import RxJs required methods
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';

@Injectable()
export class HttpRequester {
    
    // Resolve HTTP using the constructor
    constructor(private httpClient: HttpClient, private http: Http) { }

    storeDetails(){
        
    }

    post(url: string, authorization: string, body: Object): Observable<any> {
        //debugger
        let headers
        if(authorization !==null && authorization!=="")
        {
            headers = new Headers({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'oauth-token': authorization
            }); // ... Set content type to JSON
        }
        else{
            headers = new Headers({
            'Content-Type': 'application/json',
            'Accept': 'application/json'
            // 'User-Agent': 'tytrhrgfdsytgtfrd'
        }); // ... Set content type to JSON
    }
    debugger
        const bodyString = JSON.stringify(body); // Stringify payload
        const options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(url, bodyString, options) // ...using post request
        .map((res: Response) => {
           // debugger
            if (res.status === 204) {
                return res.status
            } else {
                return res.json()
            }
        }
        )
            .catch((error: any) => Observable.throw(error.status));
    }
        
    get(url: string,authorization: string): Observable<any> {
        let headers
        debugger
        if(authorization !==null && authorization !== "")
        {
            headers = new Headers({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'oauth-token': authorization
            });  
        }
        else{

        
        headers = new Headers({
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }); // ... Set content type to JSON
    }
       
        let options = new RequestOptions({ headers: headers }); // Create a request option
       // debugger
        return this.http.get(url, options)
            .map((res: Response) => {
               // debugger
                if (res.status === 204) {
                    return res.status
                } else {
                    return res.json()
                }
            }
            )
            //...errors if any
            .catch((error: any) => Observable.throw(error.status));

    }

    delete(url: string, authorization: string ): Observable<any> {
        debugger
        let headers;
        headers = new Headers({
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'oauth-token': authorization
        });  
         // const bodyString = JSON.stringify(body); // Stringify payload
         let options = new RequestOptions({ headers: headers }); // Create a request optio
        return this.http.delete(url,options)
        .map((res: Response) => {
            // debugger
             if (res.status === 204) {
                 return res.status
             } else {
                 return res.json()
             }
         }
         )
            .catch((error: any) => Observable.throw( error.status)); //...errors if any
    }

    put(url: string, authorization: string, body: Object): Observable<any> {
        //debugger
        const bodyString = JSON.stringify(body); // Stringify payload
        console.log(bodyString);
        console.log(authorization);
        console.log(url);
       //debugger
        let headers
        if(authorization !==null && authorization !== "")
        {
            headers = new Headers({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'oauth-token': authorization
            });  
        }
        else{

        
        headers = new Headers({
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }); // ... Set content type to JSON
    }
        let options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.put(url, bodyString, options)
            .map((res: Response) => {
                if(res.status === 201){
                    return res.status;
                }else{
                    return res.json();
                }
            }
            )
            //...errors if any
            .catch((error: any) => Observable.throw(error.status));
    }

    userRegister(url: string, authorization: string, body: Object): Observable<any> {
        const bodyString = JSON.stringify(body); // Stringify payload
      // debugger
        let headers
        headers = new Headers({
            'Content-Type': 'application/json',
            'Accept': 'application/json'
           
        }); // ... Set content type to JSON
       
        let options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.put(url, bodyString, options)
            .map((res: Response) => {
                if (res.status === 201) {
                    console.log("successfull");
                    return res.status
                } else {
                    return res.json()
                }
            }
            )
            //...errors if any
            .catch((error: any) => Observable.throw(error.status));
    }

    patch(url: string, authorization: string, body: Object): Observable<any> {
        const bodyString = JSON.stringify(body);
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Device-Token': 'jksahskads',
                'Device-Type': 'WEB',
                'Authorization': authorization
            })
        };
        return this.httpClient.patch(url,bodyString,httpOptions)
        .catch((error: any) => Observable.throw(error.json().error || error.status)); //...errors if any //...errors if any
    }

    getSessionUri(url: string,authorization: string,imgExt): Observable<any> {
        debugger
        let headers
        headers = new Headers({
            'Content-Type': 'image/'+imgExt,
            'Accept': 'application/json'
        }); // ... Set content type to JSON

        let options = new RequestOptions({ headers: headers }); // Create a request option
        debugger
        return this.http.get(url, options)
            .map((res: Response) => {
                    return res.json();
            })
            //...errors if any
            .catch((error: any) => Observable.throw(error.status));

    }

    getSigned(url: string,authorization: string): Observable<any> {
        debugger
        let headers
        headers = new Headers({
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'user-agent': 'fdgdcsdvfsgfc'
        }); // ... Set content type to JSON

        let options = new RequestOptions({ headers: headers }); // Create a request option
        debugger
        return this.http.get(url, options)
            .map((res: Response) => {
                    return res.json();
            })
            //...errors if any
            .catch((error: any) => Observable.throw(error.status));
    }
}