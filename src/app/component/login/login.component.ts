import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,ReactiveFormsModule } from '@angular/forms';
import { ApiService } from '../../api.service';
import { UserRegister, PhoneNumber, UserProfile, DeviceInfo } from '../../model/register.model';
//import { Observable } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
public name: string;
public password: string;
public email: string;
public bio: string;
public date: string;
public registrationForm: FormGroup;
public user_register_model: UserRegister = new UserRegister();
public phone_number: PhoneNumber = new PhoneNumber();
public user_profile: UserProfile = new UserProfile();
public device_info: DeviceInfo = new DeviceInfo();
public details: Object;

constructor(private apiService: ApiService, private fb: FormBuilder) { 
  this.registrationForm= fb.group({name:null, email:null, password:null, country_code:null, phone_number:null, bio:null, date: null});   
}

ngOnInit() {
  //   this.api.getDetails().subscribe(api=>this.details$=api);
}
  
//   public  getDetails(){
//     this.apiService.getContacts().subscribe((data:  Array<object>) => {
//         this.contacts  =  data;
//         console.log(data);
//     });
// }


submitForm(value:any){
  this.phone_number.cc = value.country_code;
  this.phone_number.iso_alpha_2_cc = "in";
  this.phone_number.number=value.phone_number;
  this.user_profile.birth_date = ((value.date)/1000|0).toString();
  this.user_profile.name=value.name;
  this.user_profile.basic_info="smart";
  this.user_profile.video_bio_hres=value.bio;
  this.device_info.device_token="testedtoken";
  this.device_info.device_type="WEB";
  this.user_register_model.password=value.password;
  this.user_register_model.phone_number=this.phone_number;
  this.user_register_model.device_info=this.device_info;
  this.user_register_model.user_profile=this.user_profile;
  // this.name=value.name;
  // this.password=value.password;
  // this.bio=value.bio;
  // this.date=value.date;
  // this.email=value.email;
   this.apiService.signUpDetails(this.user_register_model)
   .subscribe(
    (response) => console.log(response),
    (error) => console.log(error)
   );
  console.log(JSON.stringify(this.user_register_model));
}

  // loginUser() {
  //    let loginBody={
  //      "user_name" :this.model.user_name,
  //      "email": this.model.email,
  //      "password": this.model.password,
  //      "date": this.model.date
  //    } 
  // }
}
