import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Login } from '../../model/login.model';
import { PhoneNumber, DeviceInfo } from '../../model/register.model';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  public country_code: string="+91";
  public password: string="";
  public login: Login= new Login();
  public iso_alpha_2_cc: string="in";
  public phone_number: PhoneNumber= new PhoneNumber();
  public device_info: DeviceInfo= new DeviceInfo();
  public loginForm: FormGroup;

  constructor(private apiService:ApiService, private fb: FormBuilder) { 
    this.loginForm= fb.group({country_code:null, password:null,phone_number:null});
  }

  ngOnInit() {
  }

  submitForm(values:any){
    this.phone_number.cc=values.country_code;
    this.phone_number.number=values.phone_number;
    this.phone_number.iso_alpha_2_cc=this.iso_alpha_2_cc;
    this.device_info.device_token="123";
    this.device_info.device_type="WEB";
    this.login.password=values.password;
    this.login.phone_number=this.phone_number;
    this.login.device_info=this.device_info;

    this.apiService.loginDetails(this.login).subscribe(
       (response) => console.log(response),
       (error) => console.log(error)
     );

    console.log(JSON.stringify(values));
  }


}
